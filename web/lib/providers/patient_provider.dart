import 'package:flutter/foundation.dart';
import 'package:web/models/patient.dart';
import 'package:web/services/firestore_database.dart';

class PatientProvider with ChangeNotifier {
  Patient _patient;
  FirebaseService _firebase;

  setPatient(String patientId) {
    _firebase.getPatient(patientId).then((patient) {
      _patient = patient;
      notifyListeners();
    });
  }

  Patient get patient {
    if (_patient == null)
      return null;
    return _patient.copy();
  }

  void logOut() {
    _patient = null;
    notifyListeners();
  }
}