import 'package:flutter/cupertino.dart';
import 'package:web/models/model.dart';

class Patient extends Model {
  final String name;
  final String surname;
  final String birthNumber;

  Patient({
    String id,
    @required this.name,
    @required this.surname,
    @required this.birthNumber,
  }) : super(id);

  Patient.fromMap(Map<String, Object> map)
      : name = map['name'],
        surname = map['surname'],
        birthNumber = map['birthNumber'],
        super(map['id']);

  @override
  Map<String, Object> toMap() {
    return {
      'name': name,
      'surname': surname,
      'birthNumber': birthNumber,
    };
  }

  Patient copy({
    String id,
    String name,
    String surname,
    String birthNumber,
  }) {
    return Patient(
      id: id ?? this.id,
      name: name ?? this.name,
      surname: surname ?? this.surname,
      birthNumber: birthNumber ?? this.birthNumber,
    );
  }
}
