import 'package:cloud_firestore/cloud_firestore.dart';

abstract class Model {
  final String id;

  Model(String id)    // Generates id if it is null
      : id = id ?? Firestore.instance.collection('').document().documentID;

  Map<String, Object> toMap();
}