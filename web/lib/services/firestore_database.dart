import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:web/models/model.dart';
import 'package:web/models/patient.dart';
import 'package:web/services/database_paths.dart';

class FirebaseService {

  final Firestore _db = Firestore.instance;

  Future<Patient> getPatient(String patientId) async {
    DocumentSnapshot doc = await _db.collection(DatabasePaths.patients).document(patientId).get();
    Patient patient = Patient.fromMap({
      'id': doc.documentID,
      ...doc.data,
    });
    return patient;
  }


  Future<List<Map<String,Object>>> queryAll(String collectionPath) async {
    List<Map<String,Object>> docs = [];

    var snapshot = await _db.collection(collectionPath).getDocuments();

    for (DocumentSnapshot document in snapshot.documents) {
      docs.add({
        'id': document.documentID,
        ...document.data,
      });
    }

    return docs;
  }


  Future<DocumentReference> addModel(String collectionPath, Model model) async {
    Map<String, Object> data = model.toMap();

    // model does not have an id
    if (model.id == null) {
      return await _db.collection(collectionPath).add(data);
    }

    // model has an id
    DocumentReference doc =
    _db.collection(collectionPath).document(model.id);

    await doc.setData(data);

    return doc;
  }

  Future<void> updateModel(String collectionPath, Model model) async {
    Map<String, Object> data = model.toMap();
    return await _db
        .collection(collectionPath)
        .document(model.id)
        .updateData(data);
  }

  Future<void> deleteModel(String collectionPath, Model model) async {
    return await _db
        .collection(collectionPath)
        .document(model.id)
        .delete();
  }
}
