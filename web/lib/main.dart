import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:web/providers/patient_provider.dart';
import 'package:web/views/startup_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PatientProvider>(
          create: (_) => PatientProvider(),
        )
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: StartUpView(title: 'Smart Health'),
      ),
    );
  }
}
