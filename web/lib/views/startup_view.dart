import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:web/models/patient.dart';
import 'package:web/providers/patient_provider.dart';

class StartUpView extends StatefulWidget {
  StartUpView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _StartUpViewState createState() => _StartUpViewState();
}

class _StartUpViewState extends State<StartUpView> {
  void _fetchId() {
    Future<http.Response> response = http.get('http://127.0.0.1:5000/nfc-id');
    response.then((response) {
      if (response.statusCode == 200) {
        Map<String, Object> responseJson = json.decode(response.body);
        String nfcId = responseJson['nfcId'];
        Provider.of<PatientProvider>(context, listen: false).setPatient(nfcId);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Patient patient = Provider.of<PatientProvider>(context).patient;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: patient != null
            ? Text('nacital som pacienta')
            : RaisedButton(
                onPressed: _fetchId,
                child: Text('Scan Card'),
              ),
      ),
    );
  }
}
